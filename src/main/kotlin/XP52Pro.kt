import HIDKeyCode.*
import org.apache.commons.lang3.RandomStringUtils.random

/**
 * Created by pervoliner on 08.06.2016.
 */
class XP52Pro constructor(val dcsKey: DCSKey) {

    companion object {
        val DCSKey2HIDKeyCode: Map<String, HIDKeyCode> = hashMapOf(
                "Back" to Keyboard_Backspace,
                "Space" to Keyboard_Space,
                "Up" to Keyboard_UpArrow,
                "Down" to Keyboard_DownArrow,
                "Left" to Keyboard_LeftArrow,
                "Right" to Keyboard_RightArrow,
                "Num+" to Keypad_Plus,
                "Num-" to Keypad_Minus,
                "\\\\" to Keyboard_BackSlash,
                "Esc" to Keyboard_Escape,
                "\\'" to Keyboard_SingleQuote,
                "SysRQ" to Keyboard_SysregAttention,
                "Num1" to Keypad_1,
                "Num2" to Keypad_2,
                "Num3" to Keypad_3,
                "Num4" to Keypad_4,
                "Num5" to Keypad_5,
                "Num6" to Keypad_6,
                "Num7" to Keypad_7,
                "Num8" to Keypad_8,
                "Num9" to Keypad_9,
                "Num0" to Keypad_0,
                "Num*" to Keypad_Multiply,
                "Num/" to Keypad_Slash,
                "NumEnter" to Keypad_Enter,
                "Num." to Keypad_Dot,
                "LAlt" to Keyboard_LeftAlt,
                "LShift" to Keyboard_LeftShift,
                "LCtrl" to Keyboard_LeftControl,
                "LWin" to Keyboard_LeftGUI,
                "RCtrl" to Keyboard_RightControl,
                "RShift" to Keyboard_RightShift,
                "RAlt" to Keyboard_RightAlt
        )


        const val ACTION_template = "[action device=keyboard usage=%s page=0x00000007 value=0x00000001]"
        const val COMMAN_template = "[actioncommand=%s name='%s'\n" +
                "   [actionblock\n" +
                "       %s]]"

        const val COMMAND_ID_TEMPLATE = "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX"
        val ID_PARTS: Array<String> = COMMAND_ID_TEMPLATE.split('-').toTypedArray()
    }
    //page=0x00000007 - hid keyboard page id,  value=0x00000001 - press action


    fun nextID(): String {
        val parts: Array<String> = Array(ID_PARTS.size, { i: Int ->
            random(ID_PARTS[i].length, "01234567890abcdef").toLowerCase()
        })
        return parts.joinToString(separator = "-")
    }

    fun format():String {
        var command = "[actioncommand=${nextID()} name='${dcsKey.category}:${dcsKey.name}'\n"
        command += "\t[actionblock\n"
        dcsKey.reformers.forEach {
            command += "\t\t[action device=keyboard usage=${getHIDKeyCodeBy(it)?.hex} page=0x00000007 value=0x00000001]\n"
        }
        command += "\t\t[action device=keyboard usage=${getHIDKeyCodeBy(dcsKey.key)?.hex} page=0x00000007 value=0x00000001]"
        command += "]]"
        return command
    }

    fun getHIDKeyCodeBy(key: String): HIDKeyCode? {
        val code = DCSKey2HIDKeyCode[key]
        when (code) {
            null -> return HIDKeyCode.values().find { it.char == key }
            else -> return code
        }
    }
}