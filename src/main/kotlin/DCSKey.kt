/**
 * Created by pervoliner on 06.06.2016.
 */
data class DCSKey constructor(
        val key: String,
        val reformers: Array<out String> = arrayOf<String>(),
        val name: String,
        val category: String) {
}