/**
 * Created by pervoliner on 05.06.2016.
 */

//val Categories:List<String> = listOf("Kneeboard","Flight Control","Systems","Modes","Sensors","Weapons","Countermeasures")


//

val REGEX_base = Regex("\\{(key = '.*?')(,[ \t]{1,2}reformers = \\{.*})?}.*[ \\t](name = .*\\)).*[ \\t](category = .*\\))")
val REGEX_key = Regex("key = '(.*)'")
val REGEX_reformers = Regex(", reformers = \\{(.*)\\}\\}")
val REGEX_name = Regex("name = _\\('(.*)'\\)")
val REGEX_category = Regex("category = _\\('(.*)'\\)")

val added: List<String> = mutableListOf()
val Categories: List<String> = listOf("Autopilot", "Systems", "Weapons", "Sensors", "Flight Control", "Modes")

fun main(args: Array<String>) {

    listOf("common_keyboard_binding.lua")
            .map { file: String -> (processFile(file)) }
            .flatMap { list -> list }
//            .filter { key -> Categories.contains(key.category) }
            .sortedBy { key -> key.category }
            .map { key ->
                added.plus(key.key)
                XP52Pro(key) }
            .map { key -> key.format() }
            .map { s -> println(s)}
}


private fun processFile(file: String): List<DCSKey> {
    val result = arrayListOf<DCSKey>()
    ClassLoader.getSystemClassLoader().getResourceAsStream(file).reader().forEachLine {
        val dcsKey = buildKey(input = it)
        if (dcsKey != null) result.add(dcsKey)
    }
    return result
}

fun buildKey(input: String): DCSKey? {
    val values = REGEX_base.find(input)?.groupValues
    if (values != null) {
        val dcsKey = DCSKey(key = REGEX_key.matchEntire(values[1])?.groupValues?.get(1).orEmpty(),
                reformers = toArray(REGEX_reformers.matchEntire(values[2])?.groupValues?.get(1)).orEmpty(),
                name = REGEX_name.matchEntire(values[3])?.groupValues?.get(1).orEmpty(),
                category = REGEX_category.matchEntire(values[4])?.groupValues?.get(1).orEmpty())
        return dcsKey
    }
    return null
}


fun toArray(input: String?): Array<String>? {
    return input?.split(',')?.map { it.replace("'", " ", false).trim() }?.toTypedArray()
}

//fun parseKey(input: CharSequence): String? = regex.find(input)?.groupValues?.get(1)